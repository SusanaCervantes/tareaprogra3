/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1;

import javafx.application.Application;
import javafx.stage.Stage;
import tarea1.util.FlowController;

/**
 *
 * @author Susana
 */
public class Tarea1 extends Application {
    
    @Override
    public void start(Stage primaryStage) 
    {
        FlowController.getInstance().InitializeFlow(primaryStage, null);
        FlowController.getInstance().goMain();
        FlowController.getInstance().goView("Menu");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        launch(args);
        
    }

    
}
