/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author liedu
 */
@Entity
@Table(name = "ASIGROL_PUESTO", schema = "UNA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Puesto.findAll", query = "SELECT p FROM Puesto p")
    , @NamedQuery(name = "Puesto.findByPuesId", query = "SELECT p FROM Puesto p WHERE p.puesId = :puesId")
    , @NamedQuery(name = "Puesto.findByPuesCodigo", query = "SELECT p FROM Puesto p WHERE p.puesCodigo = :puesCodigo")
    , @NamedQuery(name = "Puesto.findByPuesDescripcion", query = "SELECT p FROM Puesto p WHERE p.puesDescripcion = :puesDescripcion")
    , @NamedQuery(name = "Puesto.findByPuesNombre", query = "SELECT p FROM Puesto p WHERE p.puesNombre = :puesNombre")})
public class Puesto implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "ASIGROL_PUESTO_ID_GENERATOR", sequenceName = "una.PUESTO_SEQ02", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ASIGROL_PUESTO_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "PUES_ID")
    private Long puesId;
    @Basic(optional = false)
    @Column(name = "PUES_CODIGO")
    private String puesCodigo;
    @Basic(optional = false)
    @Column(name = "PUES_DESCRIPCION")
    private String puesDescripcion;
    @OneToMany(mappedBy = "empPuesId", fetch = FetchType.LAZY)
    private List<Empleado> empleadoList;
    @OneToMany(mappedBy = "rolPuesId", fetch = FetchType.LAZY)
    private List<Rol> rolList;
    @Basic(optional = false)
    @Column(name = "PUES_NOMBRE")
    private String puesNombre;

    public Puesto() {
    }

    public Puesto(Long puesId) {
        this.puesId = puesId;
    }

    public Puesto(Long puesId, String puesCodigo, String puesDescripcion, String puesNombre) {
        this.puesId = puesId;
        this.puesCodigo = puesCodigo;
        this.puesDescripcion = puesDescripcion;
        this.puesNombre = puesNombre;
    }
    
    public Puesto(PuestoDto puestoDto) {
        this.puesId = puestoDto.getId();
        actualizarPuesto(puestoDto);
    }

    public void actualizarPuesto(PuestoDto puestoDto) {
        this.puesCodigo = puestoDto.getCodigo();
        this.puesDescripcion = puestoDto.getDescripcion();
        this.puesNombre = puestoDto.getNombre();
    }

    public Long getPuesId() {
        return puesId;
    }

    public void setPuesId(Long puesId) {
        this.puesId = puesId;
    }

    public String getPuesCodigo() {
        return puesCodigo;
    }

    public void setPuesCodigo(String puesCodigo) {
        this.puesCodigo = puesCodigo;
    }

    public String getPuesDescripcion() {
        return puesDescripcion;
    }

    public void setPuesDescripcion(String puesDescripcion) {
        this.puesDescripcion = puesDescripcion;
    }

    @XmlTransient
    public List<Empleado> getEmpleadoList() {
        return empleadoList;
    }

    public void setEmpleadoList(List<Empleado> empleadoList) {
        this.empleadoList = empleadoList;
    }

    @XmlTransient
    public List<Rol> getRolList() {
        return rolList;
    }

    public void setRolList(List<Rol> rolList) {
        this.rolList = rolList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (puesId != null ? puesId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Puesto)) {
            return false;
        }
        Puesto other = (Puesto) object;
        if ((this.puesId == null && other.puesId != null) || (this.puesId != null && !this.puesId.equals(other.puesId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "tarea1.model.Puesto[ puesId=" + puesId + " ]";
    }

    public String getPuesNombre() {
        return puesNombre;
    }

    public void setPuesNombre(String puesNombre) {
        this.puesNombre = puesNombre;
    }
    
    
}
