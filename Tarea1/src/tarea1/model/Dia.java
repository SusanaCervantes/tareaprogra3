/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author liedu
 */
@Entity
@Table(name = "ASIGROL_DIA", schema = "UNA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Dia.findAll", query = "SELECT d FROM Dia d")
    , @NamedQuery(name = "Dia.findByDiaId", query = "SELECT d FROM Dia d WHERE d.diaId = :diaId")
    , @NamedQuery(name = "Dia.findByDiaHorainicio", query = "SELECT d FROM Dia d WHERE d.diaHorainicio = :diaHorainicio")
    , @NamedQuery(name = "Dia.findByDiaHorafin", query = "SELECT d FROM Dia d WHERE d.diaHorafin = :diaHorafin")
    , @NamedQuery(name = "Dia.findByDiaHorastrabajadas", query = "SELECT d FROM Dia d WHERE d.diaHorastrabajadas = :diaHorastrabajadas")
    , @NamedQuery(name = "Dia.findByDiaNombre", query = "SELECT d FROM Dia d WHERE d.diaNombre = :diaNombre")
    , @NamedQuery(name = "Dia.findByDiaLibre", query = "SELECT d FROM Dia d WHERE d.diaLibre = :diaLibre")})
public class Dia implements Serializable {

    @Basic(optional = false)
    @Column(name = "DIA_HORASTRABAJADAS")
    private Integer diaHorastrabajadas;

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "ASIGROL_DIA_ID_GENERATOR", sequenceName = "una.DIA_SEQ04", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ASIGROL_DIA_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "DIA_ID")
    private Long diaId;
    @Basic(optional = false)
    @Column(name = "DIA_HORAINICIO")
    private String diaHorainicio;
    @Basic(optional = false)
    @Column(name = "DIA_HORAFIN")
    private String diaHorafin;
    @Basic(optional = false)
    @Column(name = "DIA_NOMBRE")
    private String diaNombre;
    @Basic(optional = false)
    @Column(name = "DIA_LIBRE")
    private String diaLibre;
    @JoinColumn(name = "DIA_ROL_ID", referencedColumnName = "ROL_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Rol diaRolId;

    public Dia() {
    }

    public Dia(Long diaId) {
        this.diaId = diaId;
    }

    public Dia(Long diaId, String diaHorainicio, String diaHorafin, Integer diaHorastrabajadas, String diaNombre, String diaLibre) {
        this.diaId = diaId;
        this.diaHorainicio = diaHorainicio;
        this.diaHorafin = diaHorafin;
        this.diaHorastrabajadas = diaHorastrabajadas;
        this.diaNombre = diaNombre;
        this.diaLibre = diaLibre;
    }
    
        public Dia(DiaDto diaDto) {
        this.diaId = diaDto.getId();
        actualizarDia(diaDto);
    }

    public void actualizarDia(DiaDto diaDto) {
        this.diaHorainicio = diaDto.getHorainicio();
        this.diaHorafin = diaDto.getHorafin();
        this.diaHorastrabajadas = Integer.valueOf(diaDto.getHorastrabajadas());
        this.diaNombre = diaDto.getNombre();
        this.diaLibre = diaDto.getLibre();
    }

    public Long getDiaId() {
        return diaId;
    }

    public void setDiaId(Long diaId) {
        this.diaId = diaId;
    }

    public String getDiaHorainicio() {
        return diaHorainicio;
    }

    public void setDiaHorainicio(String diaHorainicio) {
        this.diaHorainicio = diaHorainicio;
    }

    public String getDiaHorafin() {
        return diaHorafin;
    }

    public void setDiaHorafin(String diaHorafin) {
        this.diaHorafin = diaHorafin;
    }

    public Integer getDiaHorastrabajadas() {
        return diaHorastrabajadas;
    }

    public void setDiaHorastrabajadas(Integer diaHorastrabajadas) {
        this.diaHorastrabajadas = diaHorastrabajadas;
    }

    public String getDiaNombre() {
        return diaNombre;
    }

    public void setDiaNombre(String diaNombre) {
        this.diaNombre = diaNombre;
    }

    public String getDiaLibre() {
        return diaLibre;
    }

    public void setDiaLibre(String diaLibre) {
        this.diaLibre = diaLibre;
    }

    public Rol getDiaRolId() {
        return diaRolId;
    }

    public void setDiaRolId(Rol diaRolId) {
        this.diaRolId = diaRolId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (diaId != null ? diaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dia)) {
            return false;
        }
        Dia other = (Dia) object;
        if ((this.diaId == null && other.diaId != null) || (this.diaId != null && !this.diaId.equals(other.diaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "tarea1.model.Dia[ diaId=" + diaId + " ]";
    }

    
}
