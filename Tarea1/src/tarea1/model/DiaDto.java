/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1.model;

import java.time.LocalDateTime;
import java.time.ZoneId;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author liedu
 */
public class DiaDto {

    public SimpleStringProperty id;
    public SimpleStringProperty horainicio;
    public SimpleStringProperty horafin;
    public SimpleStringProperty horastrabajadas;
    public SimpleStringProperty nombre;
    public SimpleStringProperty libre;
    public RolDto rol;

    public DiaDto() {
        this.id = new SimpleStringProperty();
        this.horainicio = new SimpleStringProperty();
        this.horafin = new SimpleStringProperty();
        this.horastrabajadas = new SimpleStringProperty();
        this.nombre = new SimpleStringProperty();
        this.libre = new SimpleStringProperty();
    }
    
    public void crearDia(){
        this.id = new SimpleStringProperty();
        this.horainicio = new SimpleStringProperty();
        this.horafin = new SimpleStringProperty();
        this.horastrabajadas = new SimpleStringProperty();
        this.nombre = new SimpleStringProperty();
        this.libre = new SimpleStringProperty();
    }

    public DiaDto(Dia dia) {
        crearDia();
        this.id.set(dia.getDiaId().toString());
        this.horainicio.set(dia.getDiaHorainicio());
        this.horafin.set(dia.getDiaHorafin());
        this.horastrabajadas.set(dia.getDiaHorastrabajadas().toString());
        this.nombre.set(dia.getDiaNombre());
        this.libre.set(dia.getDiaLibre());
        //this.rol = new RolDto(dia.getDiaRolId());
    }

    public Long getId() {
         if(id.get()!=null&&!id.get().isEmpty()){
            return Long.valueOf(this.id.get());
        }
        else{
            return null;
        }
    }

    public void setId(Long Id) {
        this.id.set(Id.toString());
    }

    public String getHorainicio() {
        return horainicio.get();
    }

    public void setHorainicio(String Horainicio) {
        this.horainicio.set(Horainicio);
    }

    public String getHorafin() {
        return horafin.get();
    }

    public void setHorafin(String Horafin) {
        this.horafin.set(Horafin);
    }

    public String getHorastrabajadas() {
        return horastrabajadas.get();
    }

    public void setHorastrabajadas(String Horastrabajadas) {
        this.horastrabajadas.set(Horastrabajadas);
    }

    public String getNombre() {
        return nombre.get();
    }

    public void setNombre(String Nombre) {
        this.nombre.set(Nombre);
    }

    public String getLibre() {
        return libre.get();
    }

    public void setLibre(String Libre) {
        this.libre.set(Libre);
    }

    public RolDto getRol() {
        return rol;
    }

    public void setRol(RolDto rol) {
        this.rol = rol;
    }
    
    
    
}
