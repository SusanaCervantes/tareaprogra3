/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1.model;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author liedu
 */
public class EmpleadoDto {
    public SimpleStringProperty id;
    public SimpleStringProperty nombre;
    public SimpleStringProperty apellidos;
    public SimpleStringProperty cedula;
    public SimpleIntegerProperty folio;
    public SimpleStringProperty correo;
    public PuestoDto puesto;
    public RolDto rol;

    public EmpleadoDto() {
        this.id = new SimpleStringProperty();
        this.nombre = new SimpleStringProperty();
        this.apellidos = new SimpleStringProperty();
        this.cedula = new SimpleStringProperty();
        this.folio = new SimpleIntegerProperty();
        this.correo = new SimpleStringProperty();
    }
    
    public void crearEmpleadoDto()
    {
        this.id = new SimpleStringProperty();
        this.nombre = new SimpleStringProperty();
        this.apellidos = new SimpleStringProperty();
        this.cedula = new SimpleStringProperty();
        this.folio = new SimpleIntegerProperty();
        this.correo = new SimpleStringProperty();
    }

    public EmpleadoDto(Empleado empleado) 
    {
        crearEmpleadoDto();
        this.id.set(empleado.getEmpId().toString());
        this.nombre.set(empleado.getEmpNombre());
        this.apellidos.set(empleado.getEmpApellidos());
        this.cedula.set(empleado.getEmpCedula());
        this.folio.set(empleado.getEmpFolio());
        this.correo.set(empleado.getEmpCorreo());
        if(empleado.getEmpPuesId() != null)
            this.puesto = new PuestoDto(empleado.getEmpPuesId());
        if(empleado.getEmpRolId() != null)
            this.rol = new RolDto(empleado.getEmpRolId());
    }

    public Long getId() {
        if(id.get()!=null&&!id.get().isEmpty()){
            return Long.valueOf(this.id.get());
        }
        else{
            return null;
        }
    }

    public void setId(Long Id) {
        this.id.set(Id.toString());
    }

    public String getNombre() {
        return nombre.get();
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public String getApellidos() {
        return apellidos.get();
    }

    public void setApellidos(String apellidos) {
        this.apellidos.set(apellidos);
    }

    public String getCedula() {
        return cedula.get();
    }

    public void setCedula(String cedula) {
        this.cedula.set(cedula);
    }

    public Integer getFolio() {
        return folio.get();
    }

    public void setFolio(Integer folio) {
        this.folio.set(folio);
    }

    public String getCorreo() {
        return correo.get();
    }

    public void setCorreo(String correo) {
        this.correo.set(correo);
    }

    public PuestoDto getPuesto() {
        return puesto;
    }

    public void setPuesto(PuestoDto puesto) {
        this.puesto = puesto;
    }

    public RolDto getRol() {
        return rol;
    }

    public void setRol(RolDto rol) {
        this.rol = rol;
    }
    
}
