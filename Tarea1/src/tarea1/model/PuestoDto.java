/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1.model;

import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author liedu
 */
public class PuestoDto {
    public SimpleStringProperty id;
    public SimpleStringProperty codigo;
    public SimpleStringProperty descripcion;
    public SimpleStringProperty nombre;
    public List<RolDto> roles;

    public PuestoDto() {
        this.id = new SimpleStringProperty();
        this.codigo = new SimpleStringProperty();
        this.descripcion = new SimpleStringProperty();
        this.nombre = new SimpleStringProperty();
        this.roles = new ArrayList<>();
    }
    
    public void crearPuestoDto() {
        this.id = new SimpleStringProperty();
        this.codigo = new SimpleStringProperty();
        this.descripcion = new SimpleStringProperty();
        this.nombre = new SimpleStringProperty();
        this.roles = new ArrayList<>();
    }

    public PuestoDto(Puesto puesto) {
        crearPuestoDto(); 
        this.codigo.set(puesto.getPuesCodigo());
        this.descripcion.set(puesto.getPuesDescripcion());
        this.nombre.set(puesto.getPuesNombre());
        this.id.set(puesto.getPuesId().toString());
        if(puesto.getRolList() != null){
            for(Rol r :puesto.getRolList()){
                this.roles.add(new RolDto(r));
            }
        }
    }

    public Long getId() {
        if(id.get()!=null&&!id.get().isEmpty()){
            return Long.valueOf(this.id.get());
        }
        else{
            return null;
        }
    }

    public void setId(Long id) {
        this.id.set(id.toString());
    }

    public String getCodigo() {
        return codigo.get();
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    public String getDescripcion() {
        return descripcion.get();
    }

    public void setDescripcion(String descripcion) {
        this.descripcion.set(descripcion);
    }

    public String getNombre() {
        return nombre.get();
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }
    
        @Override
    public String toString() {
        return nombre.get();
    }

    public List<RolDto> getRoles() {
        return roles;
    }

    public void setRoles(List<RolDto> roles) {
        this.roles = roles;
    }
    
    
}
