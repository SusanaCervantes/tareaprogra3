/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1.service;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Query;
import tarea1.model.Puesto;
import tarea1.model.PuestoDto;
import tarea1.util.AppContext;
import tarea1.util.ControlBD;

/**
 *
 * @author Susana
 */
public class PuestoService {
    private ControlBD cbd;
    
    public PuestoService(){
        cbd = (ControlBD) AppContext.getInstance().get("ControlBD");
    }
    
    public void InsertarPuesto(PuestoDto puestoDto)
    {
        Puesto puesto = new Puesto(puestoDto);
        try
        {
            if(puesto.getPuesId()==null)
            {
                    cbd.getEt().begin();
                    cbd.getEm().persist(puesto);
                    cbd.getEt().commit();
            }
            cbd.getEt().begin();
            cbd.getEm().merge(puesto);
            cbd.getEt().commit();
        }
        catch(Exception ex)
        {
            System.out.println(ex);
        }
		
    }
    
    public List<PuestoDto> BuscarPuesto(String nombre)
    {
        List<PuestoDto> puestoDto = new ArrayList();
        List<Puesto> puestos = new ArrayList();
        try
        {
            Query query=cbd.getEm().createNamedQuery("Puesto.findByPuesNombre",Puesto.class);
            query.setParameter("puesNombre",nombre);
            puestos = query.getResultList();
            
            for(Puesto p: puestos){
                puestoDto.add(new PuestoDto(p));
            }
            return puestoDto;
        }
        catch(Exception ex)
        {
            System.out.println("exception"+ex);
        }
        return null;
    }
    public void EliminarPuesto(String nombre)
    {
        Puesto puesto = new Puesto();
        
        try
        {
            Query query = cbd.getEm().createNamedQuery("Puesto.findByPuesNombre",Puesto.class);
            query.setParameter("puesNombre",nombre);
            puesto = (Puesto)query.getSingleResult();
            
            cbd.getEt().begin();
            cbd.getEm().remove(puesto);
            cbd.getEt().commit();
            
        }
        catch(Exception ex)
        {
            System.out.println("Excepcion "+ ex);
        }
    }
    
    public List<PuestoDto> getPuestos()
    {
        List<PuestoDto> puestoDto = new ArrayList();
        List<Puesto> puestos;
        try
        {
            Query query=cbd.getEm().createNamedQuery("Puesto.findAll",Puesto.class);
            puestos = query.getResultList();
            for(Puesto p: puestos){
                puestoDto.add(new PuestoDto(p));
            }
            return puestoDto;
        }
        catch(Exception ex)
        {
            System.out.println("exception"+ex);
        }
        return null;
    }
}
