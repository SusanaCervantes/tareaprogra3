/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1.service;

import java.util.List;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javax.persistence.Query;
import tarea1.model.Dia;
import tarea1.model.DiaDto;
import tarea1.model.Puesto;
import tarea1.model.PuestoDto;
import tarea1.model.Rol;
import tarea1.model.RolDto;
import tarea1.util.AppContext;
import tarea1.util.ControlBD;
import tarea1.util.Message;

/**
 *
 * @author liedu
 */
public class RolesService {

    ControlBD ctrl;

    public RolesService() {
        ctrl = (ControlBD) AppContext.getInstance().get("ControlBD");
    }

    public void guardarRoles(RolDto rolDto, PuestoDto puestoDto) {
        Rol rol = new Rol(rolDto);
        Puesto puesto;
        puesto = ctrl.getEm().find(Puesto.class, puestoDto.getId());
        rol.setRolPuesId(puesto);
        try {
            if (rol.getRolId() == null) {
                ctrl.getEt().begin();
                ctrl.getEm().persist(rol);
                ctrl.getEt().commit();
            } else {
                ctrl.getEt().begin();
                ctrl.getEm().merge(rol);
                ctrl.getEt().commit();
            }
            guardarDias(rol, rolDto.getDias());
        } catch (Exception e) {
            System.out.println("Eror al guardar rol" + e.getMessage());
        }

    }

    public void guardarDias(Rol rol, List<DiaDto> ldia) {
        List<DiaDto> dias = ldia;
        Dia dia;
        try {
            for (DiaDto dia1 : dias) {
                dia = new Dia(dia1);
                dia.setDiaRolId(rol);
                if (dia.getDiaId() == null) {
                    ctrl.getEt().begin();
                    ctrl.getEm().persist(dia);
                    ctrl.getEt().commit();
                } else {
                    ctrl.getEt().begin();
                    ctrl.getEm().merge(dia);
                    ctrl.getEt().commit();
                }

            }
        } catch (Exception e) {
        }
    }

    public void elininarRol(RolDto rolDto) {

    }

    public RolDto buscarRol(Integer code) {
        RolDto rol;
        Rol eRol;
        try {
            Query query = ctrl.getEm().createNamedQuery("Rol.findByRolNumero", Rol.class);
            query.setParameter("rolNumero", code);
            eRol = (Rol) query.getSingleResult();
            rol = new RolDto(eRol);
            return rol;
        } catch (Exception e) {
            new Message().show(Alert.AlertType.ERROR, "Error al consultar", e.getMessage());
            return null;
        }
    }
}
