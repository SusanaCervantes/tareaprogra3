/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import proyectoredes.controller.Controller;
import tarea1.model.EmpleadoDto;
import tarea1.service.EmpleadoService;
import tarea1.util.AppContext;
import tarea1.util.FlowController;
import tarea1.util.Message;

/**
 * FXML Controller class
 *
 * @author Arlick
 */
public class MantEmpleadosController extends Controller implements Initializable {

    @FXML
    private JFXTextField txt_nombre;
    @FXML
    private JFXTextField txt_apellidos;
    @FXML
    private JFXTextField txt_correo;
    @FXML
    private JFXTextField txt_cedula;
    @FXML
    private JFXTextField txt_folio;
    @FXML
    private JFXButton btnGuardar;
    @FXML
    private JFXTextField txt_nombre1;
    @FXML
    private TableView<EmpleadoDto> tbEmp;
    @FXML
    private TableColumn<EmpleadoDto, String> tcNombre;
    @FXML
    private TableColumn<EmpleadoDto, String> tcApellidos;
    @FXML
    private TableColumn<EmpleadoDto, String> tcCedula;
    @FXML
    private TableColumn<EmpleadoDto, String> tcCorreo;
    
    Message mensaje;
    EmpleadoService empS;
    EmpleadoDto e;
    ObservableList <EmpleadoDto> empleados;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
        mensaje = new Message();
        empS = new EmpleadoService();
        e = new EmpleadoDto();
        
        tcNombre.setCellValueFactory(x->new SimpleStringProperty(x.getValue().getNombre()));
        tcApellidos.setCellValueFactory(x->new SimpleStringProperty(x.getValue().getApellidos()));
        tcCedula.setCellValueFactory(x->new SimpleStringProperty(x.getValue().getCedula()));
        tcCorreo.setCellValueFactory(x->new SimpleStringProperty(x.getValue().getCorreo()));
        
        empleados =  FXCollections.observableArrayList();
        
        AppContext.getInstance().set("EmpleadoService", empS);
    }    

    @Override
    public void initialize() {
        
    }

    @FXML
    private void evtAtras(ActionEvent event) 
    {
        FlowController.getInstance().goMain();
        FlowController.getInstance().goView("Menu");
        Limpiar();
    }

    @FXML
    private void evtLimpiar(ActionEvent event) throws IOException 
    {
       Limpiar();
    }

    @FXML
    private void evtGuardar(ActionEvent event) 
    { 
        if(!txt_nombre1.getText().isEmpty() && !txt_apellidos.getText().isEmpty() && !txt_cedula.getText().isEmpty() && !txt_correo.getText().isEmpty() && !txt_folio.getText().isEmpty())
        {
            e.setNombre(txt_nombre1.getText());
            e.setApellidos(txt_apellidos.getText());
            e.setCedula(txt_cedula.getText());
            e.setCorreo(txt_correo.getText());
            e.setFolio(Integer.parseInt(txt_folio.getText()));
            empS.GuardarEmpleado(e);
            Limpiar();
        }
        else
        {
            mensaje.show(Alert.AlertType.INFORMATION,"","Debe ingresar completar todos los espacios para guardar al empleado");
        }
    }

    @FXML
    private void evtBuscarEmpleado(ActionEvent event) 
    {
        if(!txt_nombre.getText().isEmpty())
        {
           empleados = (FXCollections.observableArrayList(empS.buscarEmpleado(txt_nombre.getText())));
           tbEmp.setItems(empleados);
        }
        else
        {
           mensaje.show(Alert.AlertType.INFORMATION,"","Debe ingresar el nombre para buscar");
        }
    }
    
    private void Limpiar()
    {
        txt_nombre1.clear();
        txt_nombre.clear();
        txt_apellidos.clear();
        txt_correo.clear();
        txt_cedula.clear();
        txt_folio.clear();
        tbEmp.getItems().clear();
    }

    @FXML
    private void evtEliminar(ActionEvent event) 
    {
        if(!txt_nombre.getText().isEmpty())
        {
            empS.EliminarEmpleado(txt_nombre.getText());
        }
        else
        {
           mensaje.show(Alert.AlertType.INFORMATION,"","Debe ingresar el nombre para eliminar");
        }
        
    }

    @FXML
    private void evtTableColunm(MouseEvent event) {
        if(tbEmp.getSelectionModel().getSelectedItem()!=null){
            e = tbEmp.getSelectionModel().getSelectedItem();
            txt_nombre1.setText(e.getNombre());
            txt_apellidos.setText(e.getApellidos());
            txt_cedula.setText(e.getCedula());
            txt_correo.setText(e.getCorreo());
            txt_folio.setText(""+e.getFolio());
        }
    }
    
}
