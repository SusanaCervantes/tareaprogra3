/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import proyectoredes.controller.Controller;
import tarea1.model.Puesto;
import tarea1.util.AppContext;
import tarea1.util.ControlBD;
import tarea1.util.FlowController;

/**
 * FXML Controller class
 *
 * @author Susana
 */
public class MenuController extends Controller implements Initializable {


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        ControlBD cbd = new ControlBD(); 
        AppContext.getInstance().set("ControlBD", cbd);
    }    

    @Override
    public void initialize() {
        
    }

    @FXML
    private void evtEmpleados(ActionEvent event)
    {
        FlowController.getInstance().goMain();
        FlowController.getInstance().goView("MantEmpleados");
    }

    @FXML
    private void evtPuestos(ActionEvent event) 
    {
        FlowController.getInstance().goMain();
        FlowController.getInstance().goView("MantPuestos");
    }

    @FXML
    private void evtRoles(ActionEvent event) {
        FlowController.getInstance().goView("MantRoles");
    }

    @FXML
    private void evtAsignarRol(ActionEvent event) {
        FlowController.getInstance().goMain();
        FlowController.getInstance().goView("AsigRoles");
    }

    @FXML
    private void evtVisualizacion(ActionEvent event) {
        FlowController.getInstance().goMain();
        FlowController.getInstance().goView("Horarios");
    }
    
}
