/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1.controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableListBase;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import proyectoredes.controller.Controller;
import tarea1.model.DiaDto;
import tarea1.model.EmpleadoDto;
import tarea1.model.PuestoDto;
import tarea1.model.RolDto;
import tarea1.service.EmpleadoService;
import tarea1.service.PuestoService;
import tarea1.util.AppContext;
import tarea1.util.FlowController;
import tarea1.util.Message;

/**
 * FXML Controller class
 *
 * @author Susana
 */
public class HorariosController extends Controller implements Initializable {

    @FXML
    private TableView<EmpleadoDto> tblEmpleados;
    @FXML
    private TableColumn<EmpleadoDto, String> tcEmpEmpleado;
    @FXML
    private JFXComboBox<String> cbVerPor;
    @FXML
    private TableView<PuestoDto> tblPuestos;
    @FXML
    private TableColumn<PuestoDto, String> tcPuesCodigo;
    @FXML
    private TableColumn<PuestoDto, String> tcPuesPuesto;
    @FXML
    private JFXTextField tfNombre;
    @FXML
    private Label txtRolesAsig;
    @FXML
    private Label txtHorasTrab;
    @FXML
    private TableColumn<EmpleadoDto, String> tcEmpApellido;
    @FXML
    private TableView<DiaDto> tvDias;
    @FXML
    private TableColumn<DiaDto, String> tcNombre;
    @FXML
    private TableColumn<DiaDto, String> tcHoraInicio;
    @FXML
    private TableColumn<DiaDto, String> tcHoraSalida;
    @FXML
    private TableColumn<DiaDto, String> tcHorasTrabajadas;
    @FXML
    private TableColumn<DiaDto, String> tcLibre;
    
    ObservableList<EmpleadoDto> empleados;
    ObservableList<PuestoDto> puestos;
    EmpleadoService empS;
    PuestoService ps;
    HSSFWorkbook hssfwbook;
    HSSFSheet hssfsheet;
    HSSFRow hssfrow;
    CellStyle cellstyle , style;
    HSSFFont font;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        ObservableList<String> verPor = FXCollections.observableArrayList();
        empleados = FXCollections.observableArrayList();
        puestos = FXCollections.observableArrayList();
        empS = new EmpleadoService();
        ps = new PuestoService();
        
        verPor.add("Puesto");
        verPor.add("Empleado");
        cbVerPor.setItems(verPor);
        
        tcEmpEmpleado.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getNombre()));
        tcEmpApellido.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getApellidos()));
        
        tcPuesCodigo.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getCodigo()));
        tcPuesPuesto.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getNombre()));
        
        tcHoraInicio.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getHorainicio()));
        tcHoraSalida.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getHorafin()));
        tcHorasTrabajadas.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getHorastrabajadas()));
        tcNombre.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getNombre()));
        tcLibre.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getLibre()));
        
        txtRolesAsig.setText(""+empS.cantEmpleadosRol());
    }    

    @FXML
    private void evtCbVerPor(ActionEvent event) {
        if(cbVerPor.getSelectionModel().getSelectedItem() != null){
            if(cbVerPor.getSelectionModel().getSelectedItem().equals("Puesto")){
                tblEmpleados.setDisable(true);
                tblEmpleados.setVisible(false);
                
                tblPuestos.setDisable(false);
                tblPuestos.setVisible(true);
                
                tvDias.getItems().clear();
            }else
            if(cbVerPor.getSelectionModel().getSelectedItem().equals("Empleado")){
                tblEmpleados.setDisable(false);
                tblEmpleados.setVisible(true);
                
                tblPuestos.setDisable(true);
                tblPuestos.setVisible(false);
                
                tvDias.getItems().clear();
            }   
        }
    }

    @FXML
    private void btnBuscar(ActionEvent event) {
        if(cbVerPor.getSelectionModel().getSelectedItem() != null && !tfNombre.getText().isEmpty()){
            if(cbVerPor.getSelectionModel().getSelectedItem().equals("Empleado")){
                empleados = (FXCollections.observableArrayList(empS.buscarEmpleado(tfNombre.getText())));
                tblEmpleados.setItems(empleados);
            }else
            if(cbVerPor.getSelectionModel().getSelectedItem().equals("Puesto")){
                puestos = (FXCollections.observableArrayList(ps.BuscarPuesto(tfNombre.getText())));
                tblPuestos.setItems(puestos);
            }
        }else{
            if(tfNombre.getText().isEmpty()){
                new Message().show(Alert.AlertType.INFORMATION, "", "Debe escribir un criterio de busqueda");
            }
            if(cbVerPor.getSelectionModel().getSelectedItem() == null){
                new Message().show(Alert.AlertType.INFORMATION, "", "Debe seleccionar el tipo de busqueda");
            }
        }
    }

    @FXML
    private void btnAtras(ActionEvent event) {
        FlowController.getInstance().goMain();
        FlowController.getInstance().goView("Menu");
        limpiar();
    }

    @FXML
    private void btnLimpiar(ActionEvent event) {
        limpiar();
    }
    
    private void limpiar(){
        tblEmpleados.getItems().clear();
        tblPuestos.getItems().clear();
        tvDias.getItems().clear();
        
        tfNombre.clear();
        cbVerPor.getSelectionModel().clearSelection();
    }

    @Override
    public void initialize() {
        
    }

    @FXML
    private void evtTblEmpleados(MouseEvent event) {
        if(cbVerPor.getSelectionModel().getSelectedItem() != null && tblEmpleados.getSelectionModel().getSelectedItem() != null){
            if(tblEmpleados.getSelectionModel().getSelectedItem().getRol() != null){
                List<RolDto> p = new ArrayList<>();
                p.add(tblEmpleados.getSelectionModel().getSelectedItem().getRol());
                txtHorasTrab.setText(""+empS.horasTrabajadas(tblEmpleados.getSelectionModel().getSelectedItem().getRol()));
                llenarTabla(p);
            }else{
                new Message().show(Alert.AlertType.INFORMATION, "", "El empleado no tiene roles asignados");
                tvDias.getItems().clear();
            }
        }
    }

    @FXML
    private void evtTblPuestos(MouseEvent event) {
        if(cbVerPor.getSelectionModel().getSelectedItem() != null && tblPuestos.getSelectionModel().getSelectedItem() != null){
            if(tblPuestos.getSelectionModel().getSelectedItem().getRoles() != null){
                List<RolDto> p = tblPuestos.getSelectionModel().getSelectedItem().getRoles();
                txtHorasTrab.setText(""+empS.horasTrabajadasList(tblPuestos.getSelectionModel().getSelectedItem().getRoles()));
                llenarTabla(p);
            }else{
                new Message().show(Alert.AlertType.INFORMATION, "", "El puesto no tiene roles asignados");
                tvDias.getItems().clear();
            }
        }else{
            new Message().show(Alert.AlertType.WARNING, "", "Debe seleccionar el tipo de busqueda");
        }
    }

    @FXML
    private void evtTblDias(MouseEvent event) {
    }
    
    private void llenarTabla(List<RolDto> roles){
        ObservableList<DiaDto>dias = FXCollections.observableArrayList();
        
        for(RolDto rol: roles){
            System.out.println(rol.getNombre()+rol.getId());
            if(rol.getDias() != null)
                for(DiaDto dia: rol.getDias()){
                    dias.add(dia);
                }
        }
        
        tvDias.setItems(dias);
    }

    @FXML
    private void evtVerTodos(ActionEvent event) {
        if(cbVerPor.getSelectionModel().getSelectedItem() != null){
            if(cbVerPor.getSelectionModel().getSelectedItem().equals("Puesto")){
                puestos = (FXCollections.observableArrayList(ps.getPuestos()));
                tblPuestos.setItems(puestos);
            }else{
                if(cbVerPor.getSelectionModel().getSelectedItem().equals("Empleado")){
                    empleados = (FXCollections.observableArrayList(empS.getEmpleados()));
                    tblEmpleados.setItems(empleados);
                }
            }
        }else{
            new Message().show(Alert.AlertType.WARNING, "", "Debe seleccinar el criterio de busqueda");
        }
    }

    @FXML
    private void evtExportar(ActionEvent event) throws IOException
    {
        exportar();
    }
    public void exportar() throws IOException
    {
        hssfwbook = new HSSFWorkbook();
	hssfsheet = hssfwbook.createSheet();
	hssfwbook.setSheetName(0,"Reporte de Consultas");
        cellstyle = hssfwbook.createCellStyle();
	font = hssfwbook.createFont();
	font.setBold(true);
	cellstyle.setFont(font);
	style = hssfwbook.createCellStyle();
        style.setFillForegroundColor(IndexedColors.BLUE.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);

	
	String[] enc =new String[]
        {
	  "Codigo" ,"Puesto" , "Dia" , "Inicio" , "Salida" , "Horas Trabajadas" ,"Libre"
	};
        String[] emp =new String[]
        {
	  "Nombre" ,"Apellido" , "Dia" , "Inicio" , "Salida" , "Horas Trabajadas" ,"Libre"
	};

	if(tblPuestos.isVisible() && tblPuestos.getItems()!= null)
        {
            hssfrow = hssfsheet.createRow(0);
            for (int i = 0; i < enc.length; ++i) 
            {
                String en = enc[i];
                HSSFCell cell = hssfrow.createCell(i);
                cell.setCellStyle(cellstyle);
                cell.setCellValue(en);
                cell.setCellStyle(style);
            }
            for(int i = 0; i <puestos.size(); i++)
            {
                hssfrow = hssfsheet.createRow(i+1);
                if(puestos.get(i) != null && tblPuestos.getSelectionModel().getSelectedItem()!=null)
                {
                    hssfrow.createCell(0).setCellValue(tblPuestos.getSelectionModel().getSelectedItem().getCodigo());
                    hssfrow.createCell(1).setCellValue(tblPuestos.getSelectionModel().getSelectedItem().getNombre());
                    hssfrow.createCell(2).setCellValue(tcNombre.getCellData(i));
                    hssfrow.createCell(3).setCellValue(tcHoraInicio.getCellData(i));
                    hssfrow.createCell(4).setCellValue(tcHoraSalida.getCellData(i));
                    hssfrow.createCell(5).setCellValue(tcHorasTrabajadas.getCellData(i));
                    hssfrow.createCell(6).setCellValue(tcLibre.getCellData(i));
                    
                    File file = new File("Reporte.xls");
                    hssfwbook.write(file);
                    Desktop.getDesktop().open(file);  
                }
                else
                {
                    
                }
            }
        }
        else 
        {
            if(tblEmpleados.isVisible() && tblEmpleados.getItems() != null)
            {
                 hssfrow = hssfsheet.createRow(0);
            for (int i = 0; i < enc.length; ++i) 
            {
                String em = emp[i];
                HSSFCell cell = hssfrow.createCell(i);
                cell.setCellStyle(cellstyle);
                cell.setCellValue(em);
                cell.setCellStyle(style);
            }
            for(int i = 0; i <empleados.size(); i++)
            {
                hssfrow = hssfsheet.createRow(i+1);
                if(empleados.get(i) != null && tblEmpleados.getSelectionModel().getSelectedItem()!= null)
                {
                    hssfrow.createCell(0).setCellValue(tblEmpleados.getSelectionModel().getSelectedItem().getNombre());
                    hssfrow.createCell(1).setCellValue(tblEmpleados.getSelectionModel().getSelectedItem().getApellidos());
                    hssfrow.createCell(2).setCellValue(tcNombre.getCellData(i));
                    hssfrow.createCell(3).setCellValue(tcHoraInicio.getCellData(i));
                    hssfrow.createCell(4).setCellValue(tcHoraSalida.getCellData(i));
                    hssfrow.createCell(5).setCellValue(tcHorasTrabajadas.getCellData(i));
                    hssfrow.createCell(6).setCellValue(tcLibre.getCellData(i));
                }
            }
            }
        }
           
       }
        
    
    
}
