/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import proyectoredes.controller.Controller;
import tarea1.model.DiaDto;
import tarea1.model.RolDto;
import tarea1.util.AppContext;

/**
 * FXML Controller class
 *
 * @author Susana
 */
public class HorarioDiasController extends Controller implements Initializable {

    @FXML
    private TableView<DiaDto> tvDias;
    @FXML
    private TableColumn<DiaDto, String> tcNombre;
    @FXML
    private TableColumn<DiaDto, String> tcHoraInicio;
    @FXML
    private TableColumn<DiaDto, String> tcHoraSalida;
    @FXML
    private TableColumn<DiaDto, String> tcHorasTrabajadas;
    @FXML
    private Label txtHorasTrab;
    @FXML
    private Label txtTitulo;

    List<RolDto> roles;
    ObservableList<DiaDto> dias;
    @FXML
    private TableColumn<DiaDto, String> tcLibre;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        dias = FXCollections.observableArrayList();
        roles = new ArrayList<>();
        roles = (List<RolDto>) AppContext.getInstance().get("horarioRoles");
        List <DiaDto> diasDto = new ArrayList<>();
        
        for(RolDto rol: roles){
            System.out.println(rol.getNombre()+rol.getId());
            if(rol.getDias() != null)
                for(DiaDto dia: rol.getDias()){
                    dias.add(dia);
                }
        }
        
        
        tcHoraInicio.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getHorainicio()));
        tcHoraSalida.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getHorafin()));
        tcHorasTrabajadas.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getHorastrabajadas()));
        tcNombre.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getNombre()));
        tcLibre.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getLibre()));
        
        tvDias.setItems(dias);
    }    

    @Override
    public void initialize() {
        
    }

    @FXML
    private void evtTblDias(MouseEvent event) {
    }

    @FXML
    private void btnAtras(ActionEvent event) {
    }
    
}
