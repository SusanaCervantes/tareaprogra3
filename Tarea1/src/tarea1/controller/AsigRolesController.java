/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1.controller;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import proyectoredes.controller.Controller;
import tarea1.model.EmpleadoDto;
import tarea1.model.PuestoDto;
import tarea1.model.RolDto;
import tarea1.service.EmpleadoService;
import tarea1.service.PuestoService;
import tarea1.util.FlowController;
import tarea1.util.Message;

/**
 * FXML Controller class
 *
 * @author Susana
 */
public class AsigRolesController extends Controller implements Initializable {

    @FXML
    private JFXTextField tfEmpledo;
    @FXML
    private JFXTextField tfPuesto;
    @FXML
    private TableView<EmpleadoDto> tblEmpleado;
    @FXML
    private TableColumn<EmpleadoDto, String> clEmpNombre;
    @FXML
    private TableColumn<EmpleadoDto, String> clEmpApellido;
    @FXML
    private TableView<PuestoDto> tblPuesto;
    @FXML
    private TableColumn<PuestoDto, String> clPuesto;
    @FXML
    private TableView<RolDto> tblRol;
    @FXML
    private TableColumn<RolDto, String> clRolNumero;
    @FXML
    private TableColumn<RolDto, String> clRolNombre;
    @FXML
    private JFXCheckBox ckRotativo;
    @FXML
    private TableColumn<PuestoDto, String> tcCodigo;
    
    EmpleadoService empS;
    PuestoService ps;
    ObservableList <EmpleadoDto> empleados;
    ObservableList <PuestoDto> puestos; 
    ObservableList <RolDto> roles;
    PuestoDto puesto; 
    RolDto rol;
    EmpleadoDto empleado;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        clPuesto.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getNombre())); 
        tcCodigo.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getCodigo())); 
        
        clEmpNombre.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getNombre()));
        clEmpApellido.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getApellidos())); 
        
        clRolNombre.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getNombre())); 
        clRolNumero.setCellValueFactory(x -> new SimpleStringProperty(""+x.getValue().getNumero())); 
        
        empS = new EmpleadoService();// AppContext.getInstance().get("EmpleadoService");
        ps = new PuestoService();// AppContext.getInstance().get("PuestoService");

        empleados = FXCollections.observableArrayList();
        puestos = FXCollections.observableArrayList();
        roles = FXCollections.observableArrayList();
        
        puesto = new PuestoDto(); 
        rol = new RolDto(); 
        empleado = new EmpleadoDto();
    }    

    @FXML
    private void evtBuscarEmpleado(ActionEvent event) {
        if(!tfEmpledo.getText().isEmpty()){
            empleados = (FXCollections.observableArrayList(empS.buscarEmpleado(tfEmpledo.getText())));
            tblEmpleado.setItems(empleados);
        }else{
            new Message().show(Alert.AlertType.INFORMATION,"","Debe ingresar el nombre del empleado");
        }
    }

    @FXML
    private void evtBuscarPuesto(ActionEvent event) {
        if(!tfPuesto.getText().isEmpty()){
            ps.BuscarPuesto(tfPuesto.getText());
            puestos = (FXCollections.observableArrayList(ps.BuscarPuesto(tfPuesto.getText())));
            tblPuesto.setItems(puestos);
        }else{
            new Message().show(Alert.AlertType.INFORMATION,"","Debe ingresar el nombre del puesto");
        }
    }

    @FXML
    private void evtRotativo(ActionEvent event) 
    {
        if(ckRotativo.isSelected())
        {
            
        }
    }

    @FXML
    private void evtAtras(ActionEvent event) {
        FlowController.getInstance().goMain();
        FlowController.getInstance().goView("Menu");
        limpiar();
    }

    @FXML
    private void evtAsignar(ActionEvent event) {
        if(empleado!=null && puesto!=null &&rol!=null){
            empS.asignarRol(empleado, puesto, rol);
        }else{
            if(empleado==null){
                new Message().show(Alert.AlertType.WARNING,"","Debe seleccionar un empleado");
            }else{
                if(puesto==null){
                    new Message().show(Alert.AlertType.WARNING,"","Debe seleccionar un puesto");
                }else{
                    new Message().show(Alert.AlertType.WARNING,"","Debe seleccionar un rol");
                }
            }
        }
    }

    @Override
    public void initialize() {
        
    }

    @FXML
    private void evtLimpiar(ActionEvent event) {
        limpiar();
    }
    
    private void limpiar(){
        tblEmpleado.getItems().clear();
        tblPuesto.getItems().clear();
        tblRol.getItems().clear();
        
        tfEmpledo.clear();
        tfPuesto.clear();
    }

    @FXML
    private void evtTblEmpleados(MouseEvent event) {
        if(tblEmpleado.getSelectionModel().getSelectedItem() != null){
            empleado = tblEmpleado.getSelectionModel().getSelectedItem();
        }
    }

    @FXML
    private void evtTblPuesto(MouseEvent event) {
        if(tblPuesto.getSelectionModel().getSelectedItem() != null){
            puesto = tblPuesto.getSelectionModel().getSelectedItem();
            if(puesto.getRoles() != null){
                roles = (FXCollections.observableArrayList(puesto.getRoles()));
                tblRol.setItems(roles);
            }else{
                new Message().show(Alert.AlertType.INFORMATION,"","El puesto no tiene roles disponibles");
            }
        }
    }

    @FXML
    private void evtTblRol(MouseEvent event) {
        if(tblRol.getSelectionModel().getSelectedItem() != null){
            rol = tblRol.getSelectionModel().getSelectedItem();
        }
    }
}
