/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTimePicker;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import proyectoredes.controller.Controller;
import tarea1.model.DiaDto;
import tarea1.model.PuestoDto;
import tarea1.model.RolDto;
import tarea1.service.PuestoService;
import tarea1.service.RolesService;
import tarea1.util.AppContext;
import tarea1.util.FlowController;
import tarea1.util.Message;

/**
 * FXML Controller class
 *
 * @author Susana
 */
public class MantRolesController extends Controller implements Initializable {

    @FXML
    private JFXTextField txtNameRol;
    @FXML
    private JFXTextField txtNumeroRol;
    @FXML
    private TableView<DiaDto> tvDias;
    @FXML
    private TableColumn<DiaDto, String> tcNombre;
    @FXML
    private TableColumn<DiaDto, String> tcHoraInicio;
    @FXML
    private TableColumn<DiaDto, String> tcHoraSalida;
    @FXML
    private TableColumn<DiaDto, String> tcHorasTrabajadas;
    @FXML
    private JFXButton btnAgregara;
    @FXML
    private JFXButton btnGuardar;
    @FXML
    private JFXButton btnEliminar;
    @FXML
    private JFXButton btnAtras;

    DiaDto dia;
    RolDto rolDto;
    RolesService sr;
    PuestoService sp;
    PuestoDto puesto;
    ObservableList<DiaDto> dias;
    ObservableList<PuestoDto> puestos;
    Message mensaje;
    @FXML
    private JFXComboBox<PuestoDto> cbPuestos;
    @FXML
    private JFXCheckBox cbRotativo;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        puestos = FXCollections.observableArrayList();
        dias = FXCollections.observableArrayList();
        sp = new PuestoService();
        sr = new RolesService();
        rolDto= new RolDto();
        tcHoraInicio.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getHorainicio()));
        tcHoraSalida.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getHorafin()));
        tcHorasTrabajadas.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getHorastrabajadas()));
        tcNombre.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getNombre()));
        tvDias.setItems(dias);
        cargarPuestos();
    }

    @Override
    public void initialize() {
        dias = (ObservableList<DiaDto>) AppContext.getInstance().get("Semana");
        tvDias.setItems(dias);
    }

    @FXML
    private void agragar(ActionEvent event) {
        FlowController.getInstance().goView("MantDias");
    }


    @FXML
    private void guardar(ActionEvent event) {
        rolDto.setFechaSemana("45");
        rolDto.setFjo(cbRotativo.isSelected() ? "A" : "I");
        rolDto.setNombre(txtNameRol.getText());
        rolDto.setNumero(Integer.parseInt(txtNumeroRol.getText()));
        rolDto.setDias(dias);
        rolDto.setPuesto(cbPuestos.getValue());
        sr.guardarRoles(rolDto, cbPuestos.getValue());

        System.out.println(rolDto);
    }

    @FXML
    private void eliminar(ActionEvent event) {
    RolesService serv = new RolesService();
    ObservableList<DiaDto> listDias = FXCollections.observableArrayList();
        rolDto = serv.buscarRol(Integer.parseInt(txtNumeroRol.getText()));
        txtNameRol.setText(rolDto.getNombre());
        txtNumeroRol.setText(rolDto.getNumero().toString());
        cbRotativo.setSelected(rolDto.getFjo().equals("A"));
        cbPuestos.setValue(rolDto.getPuesto());
        cbPuestos.getSelectionModel().select(0);
        
        for (DiaDto listDia : rolDto.getDias()) {
            listDias.add(listDia);
        }
        dias=listDias;
        tvDias.setItems(dias);
        System.out.println(rolDto.getDias());
    
    } 

    @FXML
    private void atras(ActionEvent event) {
        FlowController.getInstance().goView("Menu");
    }
    
    private void cargarPuestos() {
        puestos.addAll(sp.getPuestos());
        cbPuestos.setItems(puestos);
    }

}
