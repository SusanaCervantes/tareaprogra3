﻿/*
Created: 6/8/2019
Modified: 6/8/2019
Model: Asignacion de Roles
Database: Oracle 11g Release 1
*/


-- Create sequences section -------------------------------------------------

CREATE SEQUENCE una.Rol_seq03
 INCREMENT BY 1
 START WITH 1
 NOMAXVALUE
 NOCACHE
;

CREATE SEQUENCE una.Empleado_seq01
 INCREMENT BY 1
 START WITH 1
 NOMAXVALUE
 NOMINVALUE
 NOCACHE
;

CREATE SEQUENCE una.Puesto_seq02
 INCREMENT BY 1
 START WITH 1
 NOMAXVALUE
 NOCACHE
;

CREATE SEQUENCE una.Dia_seq04
 INCREMENT BY 1
 START WITH 1
 NOMAXVALUE
 NOCACHE
;

-- Create tables section -------------------------------------------------

-- Table una.AsigRol_Empleado

CREATE TABLE una.AsigRol_Empleado(
  emp_Id Number NOT NULL,
  emp_Nombre Varchar2(30 ) NOT NULL,
  emp_Apellidos Varchar2(60 ) NOT NULL,
  emp_Cedula Varchar2(30 ) NOT NULL,
  emp_Folio Number NOT NULL,
  emp_Correo Varchar2(60 ) NOT NULL,
  emp_pues_id Number,
  emp_rol_Id Number
)
;

-- Add keys for table una.AsigRol_Empleado

ALTER TABLE una.AsigRol_Empleado ADD CONSTRAINT AsigRol_Empleado_PK PRIMARY KEY (emp_Id)
;

-- Table and Columns comments section

COMMENT ON COLUMN una.AsigRol_Empleado.emp_Id IS 'Id Empleados'
;
COMMENT ON COLUMN una.AsigRol_Empleado.emp_Nombre IS 'Nombre del empleado.'
;
COMMENT ON COLUMN una.AsigRol_Empleado.emp_Apellidos IS 'Apellidos del Empleado.'
;
COMMENT ON COLUMN una.AsigRol_Empleado.emp_Cedula IS 'Cedula del empleado.'
;
COMMENT ON COLUMN una.AsigRol_Empleado.emp_Folio IS 'Identificador para asignar los roles.'
;
COMMENT ON COLUMN una.AsigRol_Empleado.emp_Correo IS 'Correo Electronico del Usuario.'
;

-- Table una.AsigRol_Puesto

CREATE TABLE una.AsigRol_Puesto(
  pues_id Number NOT NULL,
  pues_Codigo Varchar2(15 ) NOT NULL,
  pues_Descripcion Varchar2(120 ) NOT NULL,
  pues_Nombre Varchar2(30 ) NOT NULL
)
;

-- Add keys for table una.AsigRol_Puesto

ALTER TABLE una.AsigRol_Puesto ADD CONSTRAINT AsigRol_Puesto_PK PRIMARY KEY (pues_id)
;

-- Table una.AsigRol_Rol

CREATE TABLE una.AsigRol_Rol(
  rol_Id Number NOT NULL,
  rol_Nombre Varchar2(30 ) NOT NULL,
  rol_Numero Number NOT NULL,
  rol_FechaSemana Varchar2(30 ) NOT NULL,
  rol_Fijo Varchar2(1 ) NOT NULL,
  rol_pues_id Number,
  CONSTRAINT AsigRol_Rol_ck01 CHECK (rol_Fijo in ('A','I'))
)
;

-- Add keys for table una.AsigRol_Rol

ALTER TABLE una.AsigRol_Rol ADD CONSTRAINT AsigRol_Rol_PK PRIMARY KEY (rol_Id)
;

-- Table and Columns comments section

COMMENT ON COLUMN una.AsigRol_Rol.rol_Id IS 'ID del rol.'
;
COMMENT ON COLUMN una.AsigRol_Rol.rol_Nombre IS 'Nombre del rol.'
;
COMMENT ON COLUMN una.AsigRol_Rol.rol_Numero IS 'Numero del rol para indicar orden.'
;
COMMENT ON COLUMN una.AsigRol_Rol.rol_FechaSemana IS 'Numero de semana de incio del rol.'
;
COMMENT ON COLUMN una.AsigRol_Rol.rol_Fijo IS 'Rol fijo: f, Rol rotativo: R'
;

-- Table una.AsigRol_Dia

CREATE TABLE una.AsigRol_Dia(
  dia_Id Number NOT NULL,
  dia_HoraInicio Varchar2(30 ) NOT NULL,
  dia_HoraFin Varchar2(30 ) NOT NULL,
  dia_HorasTrabajadas Number NOT NULL,
  dia_Nombre Varchar2(30 ) NOT NULL,
  dia_Libre Varchar2(1 ) NOT NULL,
  dia_rol_Id Number,
  CONSTRAINT AsigRol_Dia_ck01 CHECK (dia_Libre in ('S', 'N'))
)
;

-- Add keys for table una.AsigRol_Dia

ALTER TABLE una.AsigRol_Dia ADD CONSTRAINT AsigRol_Dia_PK PRIMARY KEY (dia_Id)
;

-- Table and Columns comments section

COMMENT ON COLUMN una.AsigRol_Dia.dia_Id IS 'Id dia.'
;
COMMENT ON COLUMN una.AsigRol_Dia.dia_HoraInicio IS 'Hora de inicio.'
;
COMMENT ON COLUMN una.AsigRol_Dia.dia_HoraFin IS 'Hora de Salida.'
;
COMMENT ON COLUMN una.AsigRol_Dia.dia_HorasTrabajadas IS 'Cantidad de de horas trabajadas.'
;
COMMENT ON COLUMN una.AsigRol_Dia.dia_Nombre IS 'Nombre del dia de la semana.'
;
COMMENT ON COLUMN una.AsigRol_Dia.dia_Libre IS 'Dia libre, S: si o N: no.'
;

-- Trigger for sequence una.Empleado_seq01 for column emp_Id in table una.AsigRol_Empleado ---------
CREATE OR REPLACE TRIGGER una.ts_AsigRol_Empleado_Empleado_0 BEFORE INSERT
ON una.AsigRol_Empleado FOR EACH ROW
BEGIN
  :new.emp_Id := una.Empleado_seq01.nextval;
END;
;
CREATE OR REPLACE TRIGGER una.tsu_AsigRol_Empleado_Emplead_0 AFTER UPDATE OF emp_Id
ON una.AsigRol_Empleado FOR EACH ROW
BEGIN
  RAISE_APPLICATION_ERROR(-20010,'Cannot update column emp_Id in table una.AsigRol_Empleado as it uses sequence.');
END;
;

-- Trigger for sequence una.Puesto_seq02 for column pues_id in table una.AsigRol_Puesto ---------
CREATE OR REPLACE TRIGGER una.ts_AsigRol_Puesto_Puesto_seq02 BEFORE INSERT
ON una.AsigRol_Puesto FOR EACH ROW
BEGIN
  :new.pues_id := una.Puesto_seq02.nextval;
END;
;
CREATE OR REPLACE TRIGGER una.tsu_AsigRol_Puesto_Puesto_se_0 AFTER UPDATE OF pues_id
ON una.AsigRol_Puesto FOR EACH ROW
BEGIN
  RAISE_APPLICATION_ERROR(-20010,'Cannot update column pues_id in table una.AsigRol_Puesto as it uses sequence.');
END;
;

-- Trigger for sequence una.Rol_seq03 for column rol_Id in table una.AsigRol_Rol ---------
CREATE OR REPLACE TRIGGER una.ts_AsigRol_Rol_Rol_seq03 BEFORE INSERT
ON una.AsigRol_Rol FOR EACH ROW
BEGIN
  :new.rol_Id := una.Rol_seq03.nextval;
END;
;
CREATE OR REPLACE TRIGGER una.tsu_AsigRol_Rol_Rol_seq03 AFTER UPDATE OF rol_Id
ON una.AsigRol_Rol FOR EACH ROW
BEGIN
  RAISE_APPLICATION_ERROR(-20010,'Cannot update column rol_Id in table una.AsigRol_Rol as it uses sequence.');
END;
;

-- Trigger for sequence una.Dia_seq04 for column dia_Id in table una.AsigRol_Dia ---------
CREATE OR REPLACE TRIGGER una.ts_AsigRol_Dia_Dia_seq04 BEFORE INSERT
ON una.AsigRol_Dia FOR EACH ROW
BEGIN
  :new.dia_Id := una.Dia_seq04.nextval;
END;
;
CREATE OR REPLACE TRIGGER una.tsu_AsigRol_Dia_Dia_seq04 AFTER UPDATE OF dia_Id
ON una.AsigRol_Dia FOR EACH ROW
BEGIN
  RAISE_APPLICATION_ERROR(-20010,'Cannot update column dia_Id in table una.AsigRol_Dia as it uses sequence.');
END;
;


-- Create foreign keys (relationships) section ------------------------------------------------- 

ALTER TABLE una.AsigRol_Empleado ADD CONSTRAINT AsigRol_EmpleadoPuesto_fk02 FOREIGN KEY (emp_pues_id) REFERENCES una.AsigRol_Puesto (pues_id)
;


ALTER TABLE una.AsigRol_Empleado ADD CONSTRAINT AsigRol_RolEmpleado_fk01 FOREIGN KEY (emp_rol_Id) REFERENCES una.AsigRol_Rol (rol_Id)
;


ALTER TABLE una.AsigRol_Dia ADD CONSTRAINT AsigRol_RolDia_fk04 FOREIGN KEY (dia_rol_Id) REFERENCES una.AsigRol_Rol (rol_Id)
;


ALTER TABLE una.AsigRol_Rol ADD CONSTRAINT AsigRol_PuestoRol_fk03 FOREIGN KEY (rol_pues_id) REFERENCES una.AsigRol_Puesto (pues_id)
;




-- Grant permissions section -------------------------------------------------


